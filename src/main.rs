extern crate ggez;
extern crate ggez_zgui as ui;
extern crate ggez_zscene as scene;

use ggez::conf;
use ggez::event;
use ggez::graphics::{self, Font, Image, Point2, Rect, Text};
use ggez::{Context, ContextBuilder, GameResult};
use scene::action;
use scene::{Layer, Scene, Sprite};
use std::time::Duration;

#[derive(Debug, Clone, Default)]
struct Layers {
    fg: Layer,
}

impl Layers {
    fn sorted(self) -> Vec<Layer> {
        vec![self.fg]
    }
}

#[derive(Clone, Copy, Debug)]
enum TestCommand {
    A,
    B,
    C,
    Image,
    X,
    Y,
    Z,
}

// TODO:
fn make_gui(context: &mut Context, font: &Font) -> GameResult<ui::Gui<TestCommand>> {
    let mut gui = ui::Gui::new(context);

    {
        let image = Image::new(context, "/tile.png")?;
        let button = ui::Button::new(image, 0.1, gui.sender(), TestCommand::Image);
        let anchor = ui::Anchor(ui::HAnchor::Right, ui::VAnchor::Top);
        gui.add(Box::new(button), anchor);
    }

    {
        let image = Text::new(context, "[label]", &font)?.into_inner();
        let label = ui::Label::new(image, 0.1);
        let anchor = ui::Anchor(ui::HAnchor::Left, ui::VAnchor::Bottom);
        gui.add(Box::new(label), anchor);
    }

    let v_layout_1 = {
        let image_a = Text::new(context, "[A]", font)?.into_inner();
        let image_b = Text::new(context, "[B]", font)?.into_inner();
        let image_c = Text::new(context, "[C]", font)?.into_inner();
        let button_a = ui::Button::new(image_a, 0.1, gui.sender(), TestCommand::A);
        let button_b = ui::Button::new(image_b, 0.1, gui.sender(), TestCommand::B);
        let button_c = ui::Button::new(image_c, 0.1, gui.sender(), TestCommand::C);
        let mut layout = ui::VLayout::new();
        layout.add(Box::new(button_a));
        layout.add(Box::new(button_b));
        layout.add(Box::new(button_c));
        layout
    };

    let v_layout_2 = {
        let image_i = Image::new(context, "/tile.png")?;
        let image_x = Text::new(context, "[X]", font)?.into_inner();
        let image_y = Text::new(context, "[Y]", font)?.into_inner();
        let image_z = Text::new(context, "[Z]", font)?.into_inner();
        let button_i = ui::Button::new(image_i, 0.1, gui.sender(), TestCommand::Image);
        let button_x = ui::Button::new(image_x, 0.1, gui.sender(), TestCommand::X);
        let button_y = ui::Button::new(image_y, 0.1, gui.sender(), TestCommand::Y);
        let button_z = ui::Button::new(image_z, 0.1, gui.sender(), TestCommand::Z);
        let mut layout = ui::VLayout::new();
        layout.add(Box::new(button_i));
        layout.add(Box::new(button_x));
        layout.add(Box::new(button_y));
        layout.add(Box::new(button_z));
        layout
    };

    {
        let image_a = Text::new(context, "[A]", font)?.into_inner();
        let image_b = Text::new(context, "[B]", font)?.into_inner();
        let image_i = Image::new(context, "/tile.png")?;
        let button_a = ui::Button::new(image_a, 0.1, gui.sender(), TestCommand::A);
        let button_b = ui::Button::new(image_b, 0.1, gui.sender(), TestCommand::B);
        let button_i = ui::Button::new(image_i, 0.2, gui.sender(), TestCommand::Image);
        let mut layout = ui::HLayout::new();
        layout.add(Box::new(button_a));
        layout.add(Box::new(button_i));
        layout.add(Box::new(v_layout_1));
        layout.add(Box::new(v_layout_2));
        layout.add(Box::new(button_b));
        let anchor = ui::Anchor(ui::HAnchor::Right, ui::VAnchor::Bottom);
        gui.add(Box::new(layout), anchor);
    }

    Ok(gui)
}

struct MainState {
    #[allow(dead_code)] // TODO
    font: graphics::Font,

    sprite: Sprite,
    gui: ui::Gui<TestCommand>,
    scene: Scene,
    layers: Layers,
}

impl MainState {
    fn new(context: &mut Context) -> GameResult<MainState> {
        let font = graphics::Font::new(context, "/DejaVuSerif.ttf", 24)?;

        let mut sprite = Sprite::from_path(context, "/tile.png", 0.1)?;
        sprite.set_pos(Point2::new(0.5, 0.5));

        let layers = Layers::default();
        let scene = Scene::new(layers.clone().sorted());

        // TODO: What should we do in case of adding a widget to a nested layout?
        // call `gui.resize()`?

        let gui = make_gui(context, &font)?;
        let mut this = MainState {
            font,
            sprite,
            gui,
            scene,
            layers,
        };
        this.demo_move(context)?;
        this.demo_show_hide(context)?;
        {
            let (w, h) = graphics::get_drawable_size(context);
            this.resize(context, w, h);
        }
        Ok(this)
    }

    fn demo_move(&mut self, context: &mut Context) -> GameResult<()> {
        let mut sprite = Sprite::from_path(context, "/tile.png", 0.5)?;
        sprite.set_pos(Point2::new(0.0, -1.0));
        let delta = Point2::new(0.0, 1.5);
        let move_duration = Duration::from_millis(2_000);
        let action = Box::new(action::Sequence::new(vec![
            Box::new(action::Show::new(&self.layers.fg, &sprite)),
            Box::new(action::MoveBy::new(&sprite, delta, move_duration)),
        ]));
        self.scene.add_action(action);
        Ok(())
    }

    fn demo_show_hide(&mut self, context: &mut Context) -> GameResult<()> {
        let visible = graphics::Color {
            r: 1.0,
            g: 0.0,
            b: 0.0,
            a: 1.0,
        };
        let invisible = graphics::Color { a: 0.0, ..visible };
        // let invisible = graphics::Color { r: 0.0, g: 1.0, b: 0.0, a: 1.0 };
        // let invisible = [0.0, 0.0, 0.0, 0.0];
        // let mut sprite = gui::text_sprite(context, "abc", 0.3);

        let mut sprite = {
            let mut sprite = Sprite::from_text(context, &self.font, "abc", 0.1)?;
            sprite.set_pos(Point2::new(0.0, 0.0));
            // just testing set_size method
            sprite.set_scale(2.0);
            let scale = sprite.scale();
            assert!((scale - 2.0).abs() < 0.001);
            sprite
        };

        sprite.set_color(invisible);
        let action = Box::new(action::Sequence::new(vec![
            Box::new(action::Show::new(&self.layers.fg, &sprite)),
            Box::new(action::ChangeColorTo::new(
                &sprite,
                visible,
                Duration::from_millis(1_000),
            )),
            Box::new(action::Sleep::new(Duration::from_millis(1_000))),
            Box::new(action::ChangeColorTo::new(
                &sprite,
                invisible,
                Duration::from_millis(1_000),
            )),
            Box::new(action::Hide::new(&self.layers.fg, &sprite)),
        ]));
        self.scene.add_action(action);
        Ok(())
    }

    fn resize(&mut self, context: &mut Context, w: u32, h: u32) {
        let aspect_ratio = w as f32 / h as f32;
        // let coordinates = Rect::new(0.0, 0.0, aspect_ratio, 1.0);
        let coordinates = Rect::new(-aspect_ratio, -1.0, aspect_ratio * 2.0, 2.0);
        // TODO: DO THIS IN THE GUI LIBRARY ITSELF
        graphics::set_screen_coordinates(context, coordinates).unwrap();
        self.gui.resize(aspect_ratio);
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, context: &mut Context) -> GameResult<()> {
        let dtime = ggez::timer::get_delta(context);
        self.scene.tick(dtime);
        Ok(())
    }

    fn draw(&mut self, context: &mut Context) -> GameResult<()> {
        graphics::clear(context);
        self.sprite.draw(context)?;
        self.scene.draw(context)?;
        self.gui.draw(context)?;
        graphics::present(context);
        Ok(())
    }

    fn resize_event(&mut self, context: &mut Context, w: u32, h: u32) {
        self.resize(context, w, h);
    }

    fn mouse_button_up_event(
        &mut self,
        context: &mut Context,
        _: ggez::event::MouseButton,
        x: i32,
        y: i32,
    ) {
        let window_pos = Point2::new(x as _, y as _);
        let pos = ui::window_to_screen(context, window_pos);
        let message = self.gui.click(pos);
        println!(
            "MainState::mouse_button_up_event: [{},{}] -> [{:0.2},{:0.2}]; {:?}",
            x, y, pos.x, pos.y, message
        );
    }
}

fn context() -> ggez::Context {
    let window_conf = conf::WindowSetup::default()
        .resizable(true)
        .title("Zemeroth");
    ContextBuilder::new("zemeroth", "ozkriff")
        .window_setup(window_conf)
        .add_resource_path("resources")
        .build()
        .expect("Can't build context")
}

fn main() {
    let mut context = context();
    let mut state = MainState::new(&mut context).expect("Can't create state");
    if let Err(e) = event::run(&mut context, &mut state) {
        println!("Error encountered: {}", e);
    } else {
        println!("Game exited cleanly.");
    }
}
