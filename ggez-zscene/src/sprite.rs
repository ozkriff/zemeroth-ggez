use ggez::graphics::{self, Font, Point2, Rect};
use ggez::{Context, GameResult};
use std::cell::RefCell;
use std::path::Path;
use std::rc::Rc;

#[derive(Debug, Clone)]
struct SpriteData {
    image: graphics::Image,
    basic_scale: f32,
    param: graphics::DrawParam,
}

#[derive(Debug, Clone)]
pub struct Sprite {
    data: Rc<RefCell<SpriteData>>,
}

impl Sprite {
    pub fn from_image(image: graphics::Image, height: f32) -> Self {
        let basic_scale = height / image.height() as f32;
        let param = graphics::DrawParam {
            scale: Point2::new(basic_scale, basic_scale),
            ..Default::default()
        };
        let data = SpriteData {
            image,
            param,
            basic_scale,
        };
        let data = Rc::new(RefCell::new(data));
        Self { data }
    }

    pub fn from_path<P: AsRef<Path>>(
        context: &mut Context,
        path: P,
        height: f32,
    ) -> GameResult<Self> {
        let image = graphics::Image::new(context, path)?;
        Ok(Self::from_image(image, height))
    }

    pub fn from_text(
        context: &mut Context,
        font: &Font,
        text: &str,
        height: f32,
    ) -> GameResult<Self> {
        let text = graphics::Text::new(context, text, font)?;
        Ok(Self::from_image(text.into_inner(), height))
    }

    // TODO: some method to change the image.

    pub fn draw(&self, context: &mut Context) -> GameResult<()> {
        let data = self.data.borrow();
        graphics::draw_ex(context, &data.image, data.param)
    }

    pub fn pos(&self) -> Point2 {
        self.data.borrow().param.dest
    }

    pub fn rect(&self) -> Rect {
        let data = self.data.borrow();
        let r = data.image.get_dimensions();
        // TODO: angle?
        Rect {
            x: data.param.dest.x,
            y: data.param.dest.y,
            w: r.w * data.param.scale.x,
            h: r.h * data.param.scale.y,
        }
    }

    pub fn color_opt(&self) -> Option<graphics::Color> {
        self.data.borrow().param.color
    }

    pub fn color(&self) -> graphics::Color {
        self.color_opt().unwrap()
    }

    pub fn scale(&self) -> f32 {
        let data = self.data.borrow();
        data.param.scale.x / data.basic_scale
    }

    pub fn set_pos(&mut self, pos: Point2) {
        self.data.borrow_mut().param.dest = pos;
    }

    pub fn set_color(&mut self, color: graphics::Color) {
        self.data.borrow_mut().param.color = Some(color);
    }

    pub fn set_scale(&mut self, scale: f32) {
        let mut data = self.data.borrow_mut();
        let s = data.basic_scale * scale;
        let scale = Point2::new(s, s);
        data.param.scale = scale;
    }

    // TODO: unittest this?
    pub fn is_same(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.data, &other.data)
    }
}
